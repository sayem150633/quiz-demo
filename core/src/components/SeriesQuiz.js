import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import ConnectApi from "../api/ConnectApi";
import Header from "./framework/Header";
import Footer from "./framework/Footer";

// MaterialUI
import Button from "@material-ui/core/Button";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import RadioGroup from "@material-ui/core/RadioGroup";
import { makeStyles } from "@material-ui/core/styles";
import Axios from "axios";

function similarity(item) {
    let obj = item.reduce((res, curr) => {
      if (res[curr.ques]) res[curr.ques].push(curr);
      else Object.assign(res, { [curr.ques]: [curr] });

      return res;
    }, {});
    return obj;
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  correct: {
    color: "blue",
  },
}));

export const SeriesQuiz = () => {
  const classes = useStyles();
  const { topic } = useParams();
  const API_URL = "http://127.0.0.1:8000/quiz/s/" + topic;
  const [dataState] = ConnectApi(API_URL);
  const a = dataState.data.flatMap((q) => q.answer);
  const ac = a.length;
  const [answer, setAnswer] = useState({});
    const [answerCheck, setAnswerCheck] = useState();
    
    const [Checked, setChecked] = useState([]);

  // useEffect(() => {
  //   if (Object.keys(answer).length === 0) {
  //     setAnswer(createInitalAnswers());
  //   }
  // }, [answer]);

  const handleSelection = (e) => {
    setAnswer({ ...answer, [e.target.value]: e.target.checked });
  };

  const createInitalAnswers = () => {
    let z = a.flatMap((obj) => obj.id);
    var object = {};
    for (var x = 0; x < ac; x++) {
      object[z[x]] = false;
    }
    return object;
  };

  const checkAnswer = (e) => {
    e.preventDefault();

    let n = a.map((obj) => obj.is_right);
    let y = { ...n };

    function arrayEquals(o, p) {
      return (
        Array.isArray(o) &&
        Array.isArray(p) &&
        o.length === p.length &&
        o.every((val, index) => val === p[index])
      );
    }

    let o = Object.values(y);
    let p = Object.values(answer);
    if (arrayEquals(o, p)) {
      setAnswerCheck(true);
    } else {
      setAnswerCheck(false);
    }
  };

//   function refreshPage() {
//     window.location.reload(false);
//   }
    
    const handleToggle = (Q_id, A_id) => {
      // let newChecked = [...Checked];
      // let newFood = [...foodCategoryOption];
        
        console.log(Q_id, A_id);
        
      const currentIndex = Checked.filter((item) => item.ans === A_id);
      const currentIndex2 = Checked.map((object) => object.ans).indexOf(A_id);

  
        // const currentIndex = Checked.indexOf(A_iad);
        // console.log({ Checked, currentIndex });

        // newChecked = [...Checked];
        // if (currentIndex.length === 0) {
        if (currentIndex2 === -1) {
          Checked.push({ ques: Q_id, ans: A_id });
          // setChecked(Checked);
        } else {
          // console.log(Checked, A_id);
          // console.log({ ques: Q_id, ans: A_id });
          // const currentIndex2 = Checked.map((object) => object.ans).indexOf(
          //   A_id
          // );
          // console.log(currentIndex2);
          Checked.splice(currentIndex2, 1);
          
      }
      setChecked(Checked);
      
      console.log(Checked);
    };

    console.log(Checked);

    

    const handleSubmit = () => {
        console.log(Checked);
        let b = []
        console.log(similarity(Checked));
        let data = similarity(Checked);
        let data2 = Object.entries(data);
        data2.map((item, i) => b.push(item[1] ))
            
        console.log(data2);
        console.log(b);
        Axios.post(`http://127.0.0.1:8000/quiz/check`, b).then((res) =>
          console.log(res)
        );
    }


  return (
    <React.Fragment>
      <Header />
      <Container component="main" maxWidth="xs">
        <div className={classes.paper}>
          {dataState.data.map(({ title, answer, id: Q_id }, i) => (
            <div key={i}>
              <Typography component="h1" variant="h5">
                {title}
              </Typography>
              {answer.map(({ answer_text, id }) => (
                <RadioGroup key={id}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        value={id}
                        color="primary"
                        // onChange={handleSelection}
                        onChange={() => handleToggle(Q_id, id)}
                      />
                    }
                    label={answer_text}
                  />
                </RadioGroup>
              ))}
            </div>
          ))}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            // onClick={checkAnswer}
            onClick={handleSubmit}
          >
            Submit Answer
          </Button>
        </div>
      </Container>
      <Footer />
    </React.Fragment>
  );
};

export default SeriesQuiz;
