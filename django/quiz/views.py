from itertools import count
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Question, Quizzes, Answer
from rest_framework import generics, views
from .serializers import QuizSerializer, RandomQuestionSerializer, AnswerSerializer


class StartQuiz(APIView):

    def get(self, request, **kwargs):
        quiz = Quizzes.objects.filter(category__name=kwargs['title'])
        serializer = QuizSerializer(quiz, many=True)
        return Response(serializer.data)


class Quiz(generics.ListAPIView):

    serializer_class = QuizSerializer
    queryset = Quizzes.objects.all()


class RandomQuestionTopic(APIView):

    def get(self, request, format=None, **kwargs):
        question = Question.objects.filter(quiz__title=kwargs['topic']).order_by('?')[:1]
        serializer = RandomQuestionSerializer(question, many=True)
        return Response(serializer.data)


class SeriesQuestionTopic(APIView):

    def get(self, request, format=None, **kwargs):
        question = Question.objects.filter(quiz__title=kwargs['topic'])
        serializer = RandomQuestionSerializer(question, many=True)
        return Response(serializer.data)


class CheckAnswer(views.APIView):

    def post(self, req):
        data = req.data
        score = 0
        print(data)

        for i in data:
            print(i)
            # print('testttt', i[0]['ques'])
            test = Answer.objects.filter(
                question__id=i[0]['ques'], is_right=True).count()
            # print('testttt22', test)
            if len(i) == 2 and test == 2:
                # print('ok')
                count = 0
                for j in i:
                    # print(j['ques'])
                    print(j['ans'])
                    ans = Answer.objects.filter(id=j['ans'], is_right=True)
                    if ans.exists():
                        count+=1
                    # print(ans)
                # print('count', count)
                if count == 2:
                    score = score + 1
            elif len(i) == 1 and test != 2:
                print('enter...')
                ans = Answer.objects.filter(id=i[0]['ans'], is_right=True)
                if ans.exists():
                    score = score + 1
                # print(ans)
            # for j in i:
                # b = i[j]
                # print('bbbbbb', b)
                # if b === 
                # print(j['ques'])
        
        # print(data)
        print('score', score)
        return Response('success')
